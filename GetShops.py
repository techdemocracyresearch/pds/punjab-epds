import requests
import json
from bs4 import BeautifulSoup


def write_json(new_data, filename):
    with open(filename, 'w') as file:
        json.dump(new_data, file, indent=4)

def main():
    url = "https://epos.punjab.gov.in/AjaxExecution.jsp?select=true&type=fps_id&param="

    with open("PunjabDist.json") as f:
        districts = json.load(f)

    shops_data = []
    for dist in districts:
        shop_data_district = {}
        shops = []
        response = requests.request("GET", url + str(dist['DistrictCode']))
        soup = BeautifulSoup(response.text, 'lxml')
        for fps in soup.find_all('option'):
            shops.append(fps['value'])
        shops.remove("")
        shops = list(set(shops))
        shops.sort()
        shop_data_district = { 'dist_code' : dist['DistrictCode'], 'dist_name': dist['DistrictName'], 'shops' : shops}
        shops_data.append(shop_data_district)
    write_json(shops_data, 'shops.json')

if __name__ == "__main__":
    main()